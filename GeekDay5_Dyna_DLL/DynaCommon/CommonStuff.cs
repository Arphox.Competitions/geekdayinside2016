﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
// Must add references to: PresentationCore, System.Xaml, WindowsBase
// Must use .NET 4.5.0

namespace DynaCommon
{

    /// <summary>
    /// This should be pretty self-explanatory
    /// </summary>
    public enum DynaCommand
    {
        CmdDown, CmdUp, CmdLeft, CmdRight, CmdStop, CmdBomb, CmdReverse, CmdNothing
    }

    public interface IDynaClient
    {
        /// <summary>
        /// Must return with the client's nickname
        /// </summary>
        string NickName { get; }

        /// <summary>
        /// The client will receive the remaining time through this method.
        /// Called in every 20th loop (~2 seconds)
        /// It should simply store the time left in a data field
        /// </summary>
        /// <param name="timeLeft"></param>
        void TimeLeft(int timeLeft);

        /// <summary>
        /// The client will receive the list of tiles through this method
        /// Called in every 5th loop (~ 0.5 seconds)
        /// It should simply store the level string / processed objects in a data field
        /// </summary>
        /// <param name="XMLData"></param>
        void ItemsToClient(string XMLData);

        /// <summary>
        /// The MAIN client method
        /// The client should tell the server what to do, by looking at the current map status
        /// Called once in every loop, there is a 100ms delay between loop calls
        /// MUST not use reflection or other tricks to halt/spy other players
        /// This is called in a separate thread - should not create new threads here...
        /// </summary>
        /// <param name="numberOfServerTicks">The number of server ticks (UI render cycles) since the beginning of the program</param>
        /// <returns></returns>
        DynaCommand CommandFromClient(long numberOfServerTicks);

        /// <summary>
        /// This should return the image of your client
        /// img should be non-transparent, with the size of 40x40
        /// Will be called ONCE
        /// Use relative image paths, we will put custom images next to the EXE files
        /// </summary>
        /// <returns></returns>
        BitmapImage TileOfClient();
    }
}
