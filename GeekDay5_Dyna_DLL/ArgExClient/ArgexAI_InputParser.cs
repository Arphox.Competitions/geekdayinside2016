﻿using System;
using System.Diagnostics;
using System.Xml.Linq;

namespace ArgExClient
{
    public partial class ArgexAI
    {
        public void ItemsToClientINNER(string XMLData)
        {
            #region Basic processing
            if (string.IsNullOrEmpty(XMLData))
                return;

            Map = new Map();

            XDocument xDoc = XDocument.Parse(XMLData);

            GameItem item = new GameItem();
            foreach (XElement element in xDoc.Root.Elements())
            {
                switch (Convert.ToChar(int.Parse(element.Element("TileChar").Value)))
                {
                    case 'O': PowerUp o = new PowerUp(element, PowerUpType.Simple); Map.PowerUps.Add(o); item = o; break;
                    case 'P': PowerUp p = new PowerUp(element, PowerUpType.NoClip); Map.PowerUps.Add(p); item = p; break;
                    case 'Q': PowerUp q = new PowerUp(element, PowerUpType.Armor); Map.PowerUps.Add(q); item = q; break;
                    case 'R': PowerUp r = new PowerUp(element, PowerUpType.SpeedUp); Map.PowerUps.Add(r); item = r; break;
                    case 'S': PowerUp s = new PowerUp(element, PowerUpType.NoBomb); Map.PowerUps.Add(s); item = s; break;
                    case 'T': PowerUp t = new PowerUp(element, PowerUpType.SpeedDown); Map.PowerUps.Add(t); item = t; break;
                    case 'U': PowerUp u = new PowerUp(element, PowerUpType.AlwaysBombs); Map.PowerUps.Add(u); item = u; break;
                    case 'V': item = new Wall(element); break;
                    case 'W': item = new Wall(element); break;
                    case 'X': Flame x = new Flame(element); Map.Flames.Add(x); item = x; break;
                    case 'Y': Bomb y = new Bomb(element); Map.Bombs.Add(y); item = y; break;
                    case 'Z': item = new Space(element); break;
                    default: //Player, or unknown.
                        Player player = new Player(element);
                            Map.Enemies.Add(player);
                            item = player;
                        break;
                }
                Map.Items[(int)Math.Round(item.PosX, 0), (int)Math.Round(item.PosY, 0)].Items.Add(item);
            }
            Map.MyPlayer = Map.Enemies.FindMyself();
            Map.Enemies.Remove(Map.MyPlayer);

            #region Set itemsReady to true for all elements
            for (int i = 0; i < Settings.General.MapSizeX; i++)
                for (int j = 0; j < Settings.General.MapSizeY; j++)
                    Map[i, j].ItemsReady = true;
            #endregion

            //                Logger.LogMap(Map.Items);
            //#warning Logger.LogMap(Map.Items); STAYED in code

            HaveMap = true;

            #endregion
        }
    }
}