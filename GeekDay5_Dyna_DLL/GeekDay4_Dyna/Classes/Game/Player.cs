﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;

namespace GeekDay4_Dyna.Game
{
    public enum PlayerState
    {
        Standing,
        BetweenTiles,
        InTile
    }
    public class Player : GameItem
    {
        const double INITWALKSPEED = 0.25;
        const int INITBOMBSALLOWED = 3;
        const int INITBOMBSIZE = 2;

        public const string DEFAULTNAME = "_NA_";
        public const int NOCLIPSTEPS = 2;
        public const int SECRETSTEPS = 20;
        const double SPEEDFACTOR = 0.5;

        public double WalkSpeed { get; set; }
        public int BombsAllowed { get; set; }
        public int BombSize { get; set; }

        public double CurrentWalkSpeed
        {
            get
            {
                double multiplier = 1;
                if (SpeedUpStepsRemaining > 0) multiplier += SPEEDFACTOR;
                if (SpeedDownStepsRemaining > 0) multiplier -= SPEEDFACTOR;
                return WalkSpeed * multiplier;
            }
        }

        [XmlIgnore]
        public BitmapImage Image { get; set; }

        public int BombsDetonated { get; set; }
        public int SecretsCollected { get; set; }
        public int NoClipStepsRemaining { get; set; }
        public int ArmorStepsRemaining { get; set; }
        public int SpeedUpStepsRemaining { get; set; }
        public int NoBombStepsRemaining { get; set; }
        public int SpeedDownStepsRemaining { get; set; }
        public int AlwaysBombStepsRemaining { get; set; }

        public bool MustStop { get; set; }
        public PlayerState State { get; set; }
        public int BombsLeft { get; set; }
        public int DX { get; set; } // [-1 or 1]
        public int DY { get; set; } // [-1 or 1]
        public int Died { get; set; }
        public int Kills { get; set; }

        public int SelfKills { get; set; }
        public bool IsActive { get; set; }
        public int FullyDied { get; set; }
        public int FullyKilled { get; set; }

        public int TotalPoints
        {
            get
            {
                int bombPoints = 0; /* BombsDetonated >= 50 ? 50 : BombsDetonated; */
                int powerupPoints = SecretsCollected * 10;  /* SecretsCollected >= 5 ? 25 : SecretsCollected * 5;*/
                int livesLostPoints = Died * -10; // Died * -15;
                int fullyDiedPoints = 0; // FullyDied * -75;
                int selfKillsPoints = SelfKills * -15; // SelfKills * -75;
                int killPoints = Kills * 30;
                int fullyKillPoints = 0; /* FullyKilled * 50; */
                return bombPoints + 
                        powerupPoints + 
                        livesLostPoints + 
                        fullyDiedPoints + 
                        selfKillsPoints + 
                        killPoints + 
                        fullyKillPoints;
            }
        }

        public override string ToString()
        {
            return String.Format("Nick:{0}\tSecrets:{1}\tKills:{2}\tSelfKills:{3}\tDied:{4}\tPoints:{5}\tBombs:{6}", NickName, SecretsCollected, Kills, SelfKills, Died, TotalPoints, BombsDetonated);
        }

        public void OneStep()
        {
            //if (NoClipStepsRemaining > 0) NoClipStepsRemaining--;
            if (ArmorStepsRemaining > 0) ArmorStepsRemaining--;
            if (SpeedUpStepsRemaining > 0) SpeedUpStepsRemaining--;
            if (NoBombStepsRemaining > 0) NoBombStepsRemaining--;
            if (SpeedDownStepsRemaining > 0) SpeedDownStepsRemaining--;
            if (AlwaysBombStepsRemaining > 0) AlwaysBombStepsRemaining--;
        }

        public Player()
        {
            MustStop = false;
            State = PlayerState.Standing;
            DX = 0;
            DY = 0;
            IsActive = true;

            NickName = DEFAULTNAME;
            WalkSpeed = INITWALKSPEED;
            BombsAllowed = INITBOMBSALLOWED;
            BombSize = INITBOMBSIZE;
            BombsLeft = BombsAllowed;
        }

        public override void Render(DrawingContext ctx, int size)
        {
            if (IsActive)
            {
                string s = NickName.ToLower();
                //BitmapImage bmp = Images.ContainsKey(s) ? Images[s] : Images[this.TileChar.ToString()];
                BitmapImage bmp = Image!=null ? Image : Images[this.TileChar.ToString()];
                Rect R = new Rect(PosX * size, PosY * size, size, size);
                ctx.DrawImage(bmp, R);
            }
        }

    }
}
