﻿using DynaCommon;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace GeekDay4_Dyna.Game
{
    public class GameItem 
    {
        static int IndexGenerator = 0;

        public static GameItem NewItem(char c)
        {
            switch (c)
            {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                    return new Player() { IsExplodable = true, IsSolid = false, PosX = -1, PosY = -1, TileChar = c }; // Player X
                case 'O': return new GameItem() { IsExplodable = false, IsSolid = false, PosX = -1, PosY = -1, TileChar = 'O' }; // TREASURE
                case 'P': return new GameItem() { IsExplodable = false, IsSolid = false, PosX = -1, PosY = -1, TileChar = 'P' }; // NOCLIP
                case 'Q': return new GameItem() { IsExplodable = false, IsSolid = false, PosX = -1, PosY = -1, TileChar = 'Q' }; // ARMOR
                case 'R': return new GameItem() { IsExplodable = false, IsSolid = false, PosX = -1, PosY = -1, TileChar = 'R' }; // SPEED+
                case 'S': return new GameItem() { IsExplodable = false, IsSolid = false, PosX = -1, PosY = -1, TileChar = 'S' }; // NOBOMB
                case 'T': return new GameItem() { IsExplodable = false, IsSolid = false, PosX = -1, PosY = -1, TileChar = 'T' }; // SPEED-
                case 'U': return new GameItem() { IsExplodable = false, IsSolid = false, PosX = -1, PosY = -1, TileChar = 'U' }; // ALWAYSBOMB
                case 'V': return new GameItem() { IsExplodable = true, IsSolid = true, PosX = -1, PosY = -1, TileChar = 'V' }; // EXPLODABLE WALL
                case 'W': return new GameItem() { IsExplodable = false, IsSolid = true, PosX = -1, PosY = -1, TileChar = 'W' }; // WALL
                case 'X': return new Bomb() { IsExplodable = true, IsSolid = false, PosX = -1, PosY = -1, TileChar = 'X' }; // FLAME
                case 'Y': return new Bomb() { IsExplodable = false, IsSolid = true, PosX = -1, PosY = -1, TileChar = 'Y' }; // BOMB
                case ' ':
                case 'Z': return new GameItem() { IsExplodable = false, IsSolid = false, PosX = -1, PosY = -1, TileChar = 'Z' }; // EMPTY
            }
            return null;
        }
        protected static Dictionary<string, BitmapImage> Images;
        static GameItem()
        {
            Images=new Dictionary<string,BitmapImage>();
            string path = ConfigurationManager.AppSettings["IMAGEDIR"].ToString();
            foreach (string akt in Directory.GetFiles(path))
            {
                char c = Path.GetFileName(akt)[0];
                string s = Path.GetFileNameWithoutExtension(akt);
                BitmapImage img =  new BitmapImage(new Uri(akt, UriKind.Absolute));
                Images.Add(s, img);
            }
        }
        public GameItem()
        {
            IndexGenerator++;
            this.ID = IndexGenerator;
            this.NickName = "TILE";
        }
        public virtual void Render(DrawingContext ctx, int size)
        {
            Rect R = new Rect(PosX*size, PosY*size, size, size);
            ctx.DrawImage(Images[this.TileChar.ToString()], R);
        }

        public int ID { get; set; }
        public char TileChar { get; set; }
        //public bool IsKiller { get; set; }
        //public bool IsPickable { get; set; }
        public bool IsExplodable { get; set; }
        public bool IsSolid { get; set; }
        public double PosX { get; set; }
        public double PosY { get; set; }
        public string NickName { get; set; }
    }
}
