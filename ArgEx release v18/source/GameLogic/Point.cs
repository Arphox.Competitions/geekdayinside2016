﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArgExClient
{
    class Point : IEquatable<Point>
    {
        public int X { get; set; }
        public int Y { get; set; }


        public Point(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public override string ToString()
        {
            return string.Format("X:{0} Y:{1}", X, Y);
        }

        public bool Equals(Point other)
        {
            return this.X == other.X && this.Y == other.Y;
        }
    }
}
